package br.edu.up;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Programa {

	public static void main(String[] args) {

		String url = "jdbc:sqlite:C:\\_src\\ws-qui-man\\ExSQLite\\src\\db\\primeiro_db_sqllite.db";
		
		
		try {
			
			Connection con = DriverManager.getConnection(url);
			Statement stmt = con.createStatement();
			
			Scanner leitor = new Scanner(System.in);
			
			System.out.println("Informe o nome da pessoa: ");
			String nomeEntrada = leitor.nextLine();
			
			System.out.println("Informe o cpf da pessoa: ");
			String cpfEntrada = leitor.nextLine();
			
			System.out.println("Informe o data de nascimento da pessoa: ");
			String data = leitor.nextLine();
			
			String queryInsert = "INSERT INTO pessoas (nome, cpf, dtanasc) values ('"+nomeEntrada+"', '"+cpfEntrada+"', '"+data+"')";			
			Statement stmtInsert = con.createStatement();
			stmtInsert.execute(queryInsert); //executeQuery(queryInsert);
					
//			String queryUpdate = "update pessoas set nome='Carlos Silva' where id = 3";
//			Statement stmtUpdate = con.createStatement();
//			stmtUpdate.executeUpdate(queryUpdate); //executeQuery(queryUpdate);
//			
//			String queryDelete = "delete from pessoas where id = 2";			
//			Statement stmtDelete = con.createStatement();
//			stmtDelete.execute(queryDelete); //executeQuery(queryDelete);
			
			String querySelect = "select * from pessoas";
			Statement stmtSelect = con.createStatement();			
			ResultSet resultado = stmtSelect.executeQuery(querySelect);
			
			while(resultado.next()) {
				int id = resultado.getInt(1);
				String nome = resultado.getString(2);
				String cpf = resultado.getString(3);
				String dtaNasc = resultado.getString(4);
				
				System.out.println("ID: " + id);
				System.out.println("Nome: " + nome);
				System.out.println("CPF: " + cpf);
				System.out.println("Nascimento: " + dtaNasc);
				System.out.println("-----------------");
						
			}
			
			
			
		} catch (SQLException e) {
			System.out.println("---------> ERRO: Falha na conex�o com o banco de dados!");
			System.out.println(e.getMessage());
			//e.printStackTrace();
		}
		
		

	}

}
