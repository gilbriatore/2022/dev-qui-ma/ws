package br.edu.up;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import br.edu.up.model.Pessoa;
import br.edu.up.controller.PessoaController;
import br.edu.up.view.PessoaView;

public class Programa {
	
	public static void main(String[] args) {
	
//		PessoaController controller = new PessoaController();
//		List<Pessoa> pessoas = controller.listar();
//		PessoaView.imprimirPessoas(pessoas);
		
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				mostrarPopup();
			}
			
		});
		

	}
	
	
	public static void mostrarPopup() {
		JFrame popup = new JFrame();
		popup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel texto = new JLabel("Ol� mundo!");
		popup.getContentPane().add(texto);
		
		popup.pack();
		popup.setVisible(true);
	}
}
