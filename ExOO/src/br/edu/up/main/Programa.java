package br.edu.up.main;

import java.util.ArrayList;
import java.util.List;

import br.edu.up.model.Animal;
import br.edu.up.model.Cachorro;
import br.edu.up.model.Homem;
import br.edu.up.model.Ser;

public class Programa {
	
	public static void main(String[] args) {

		//List listaDeAlgumaCoisa = new ArrayList();
		List<Object> listaDeObjetos = new ArrayList();
		List<Ser> listaDeSeres = new ArrayList();
		List<Animal> listaDeAnimais = new ArrayList();
		List<Cachorro> listaDeCachorros = new ArrayList();
		
		
		//1
		Animal objetoAnimal = new Animal();
		objetoAnimal.nome = "Tehx";
		objetoAnimal.genero = "Macho";
		objetoAnimal.porte = "Pequeno";
				
		
		try {
			objetoAnimal.setIdade(1);
		} catch (Exception e) {			
			System.out.println("Erro: " + e.toString());
		}
		
		listaDeAnimais.add(objetoAnimal);
		
		//2  //1
		Cachorro objetoCachorro = new Cachorro();
		objetoCachorro.nome = "Amora";
		objetoCachorro.genero = "F�mea";
		objetoCachorro.porte = "Pequeno";
		
		try {
			objetoCachorro.setIdade(1);
		} catch (Exception e) {			
			System.out.println("Erro: " + e.toString());
		}
		
		listaDeCachorros.add(objetoCachorro);
		listaDeAnimais.add(objetoCachorro);
		
		//3  //2
		Cachorro objetoCachorro2 = new Cachorro();
		objetoCachorro2.nome = "Jonny";
		objetoCachorro2.genero = "Macho";
		objetoCachorro2.porte = "Pequeno";
		
		try {
			objetoCachorro2.setIdade(8);
		} catch (Exception e) {			
			System.out.println("Erro: " + e.toString());
		}
		
		listaDeCachorros.add(objetoCachorro2);
		
		//4  //3
		Cachorro objetoCachorro3 = new Cachorro();
		objetoCachorro3.nome = "Zeus";
		objetoCachorro3.genero = "Macho";
		objetoCachorro3.porte = "Grande";
		
		try {
			objetoCachorro3.setIdade(3);
		} catch (Exception e) {			
			System.out.println("Erro: " + e.toString());
		}
		
		listaDeCachorros.add(objetoCachorro3);
		
		//5
		Homem objetoHomem = new Homem();
		objetoHomem.nome = "Robson";
		objetoHomem.genero = "Macho";
		objetoHomem.porte = "M�dio";
		
		try {
			objetoHomem.setIdade(250);
		} catch (Exception e) {			
			System.out.println("Erro: " + e.getMessage());
		}
		
		//listaDeCachorros.add(objetoHomem);
		listaDeAnimais.add(objetoHomem);
		
		for(Animal animal : listaDeAnimais) {
			System.out.println("Nome: " + animal.nome);
			System.out.println("G�nero: " + animal.genero);
			System.out.println("Porte: " + animal.porte);
			System.out.println("Idade: " + animal.getIdade());
			System.out.println();
		}
		
		
	}

}
